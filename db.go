package main

import (
	"bufio"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
)

type EmailDatabase interface {
	// Return the list of email recipients for a give float ID
	Lookup(floatid int) []string
}

type emailDb struct {
	table map[int][]string
}

func NewDb() *emailDb {
	db := &emailDb{}
	return db
}

func (d *emailDb) Load(rdr io.Reader) error {
	scanner := bufio.NewScanner(rdr)
	d.table = make(map[int][]string)
	for scanner.Scan() {
		if scanner.Bytes()[0] == '#' {
			continue
		}
		fields := strings.Split(scanner.Text(), ":")
		floatid, err := strconv.Atoi(fields[0])
		if err != nil {
			return err
		}
		d.table[floatid] = make([]string, 0)

		for _, email := range strings.Split(fields[1], " ") {
			if email != "" {
				d.table[floatid] = append(d.table[floatid], email)
			}
		}
	}

	return scanner.Err()
}

func (d *emailDb) Save(wtr io.Writer) error {
	keys := make([]int, 0, len(d.table))
	for k, _ := range d.table {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		_, err := fmt.Fprintf(wtr, "%d: %s\n", k, d.table[k])
		if err != nil {
			return err
		}
	}

	return nil
}

func (d *emailDb) Lookup(id int) []string {
	return d.table[id]
}
