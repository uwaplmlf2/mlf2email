// Mlf2email sends MLF2 Status messages to one or more email addresses
package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/smtp"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/mq"
	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/status"
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

const (
	SENDER  string = "mlf2-noreply@apl.uw.edu"
	SUBJECT        = "MLF2 Status"
)

type smtpConfig struct {
	Mailhost string `json:"mailhost" toml:"mailhost"`
}

type mqConfig struct {
	User     string `json:"user" toml:"user"`
	Password string `json:"password" toml:"password"`
	Host     string `json:"host" toml:"host"`
	Vhost    string `json:"vhost,omitempty" toml:"vhost,omitempty"`
	Exchange string `json:"exchange,omitempty" toml:"exchange,omitempty"`
}

type sysConfig struct {
	Mq   mqConfig   `json:"mq" toml:"mq"`
	Smtp smtpConfig `json:"smtp" toml:"smtp"`
}

type emailMessage struct {
	Text string `json:"text"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
mlf2email [options] configfile emaildbfile

Listen on an AMQP queue for MLF2 status messages and send them to the
email addresses given in the email database file.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	bindkey = flag.String("binding-key", "*.status.processed", "queue binding key")
	dryrun  = flag.Bool("dry-run", false, "don't send email")
)

func (cfg mqConfig) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s/%s",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Vhost)
}

func formatMessage(floatid int, s *status.Status) emailMessage {
	lat := status.NewDmm(s.Gps.Lat)
	lon := status.NewDmm(s.Gps.Lon)

	var hlat, hlon rune
	if lat.IsNeg() {
		hlat = 'S'
	} else {
		hlat = 'N'
	}

	if lon.IsNeg() {
		hlon = 'W'
	} else {
		hlon = 'E'
	}

	text := []string{fmt.Sprintf("Subject: %s\r\n\r\nmlf2_%d %s%c %s%c %d\r\n%s",
		SUBJECT,
		floatid,
		lat.Abs(), hlat,
		lon.Abs(), hlon,
		s.Gps.Nsat,
		s.Timestamp)}

	return emailMessage{Text: strings.Join(append(text, s.Alerts...), "\r\n")}
}

func deliverMessage(addr string, recipients []string, msg string) error {
	c, err := smtp.Dial(addr)
	if err != nil {
		return errors.Wrap(err, "SMTP connect")
	}
	defer c.Close()

	err = c.Mail(SENDER)
	if err != nil {
		return errors.Wrap(err, "SMTP mail")
	}

	for _, to := range recipients {
		err = c.Rcpt(to)
		if err != nil {
			return errors.Wrap(err, "SMTP rcpt")
		}
	}

	wc, err := c.Data()
	if err != nil {
		return errors.Wrap(err, "SMTP data")
	}
	_, err = fmt.Fprint(wc, msg)
	if err != nil {
		return errors.Wrap(err, "SMTP write body")
	}
	err = wc.Close()
	if err != nil {
		return errors.Wrap(err, "SMTP writer close")
	}

	return c.Quit()
}

func handleMessage(addr string, db EmailDatabase, deliveries <-chan amqp.Delivery) {
	for d := range deliveries {
		floatid, ok := d.Headers["floatid"].(int32)
		if !ok {
			floatid = -1
		}

		recipients := db.Lookup(int(floatid))
		if recipients == nil || len(recipients) == 0 {
			log.Printf("No recipients for float %d status", floatid)
			d.Ack(true)
			continue
		}

		var (
			s   status.Status
			err error
		)

		if d.ContentType == "application/xml" {
			err = xml.Unmarshal(d.Body, &s)
		} else {
			// JSON
			err = json.Unmarshal(d.Body, &s)
		}

		if err != nil {
			log.Printf("Cannot decode message: %v", err)
			d.Nack(false, false)
			continue
		}

		msg := formatMessage(int(floatid), &s)
		if *dryrun {
			log.Printf("Message: %#v", msg)
			d.Ack(true)
		} else {
			err = deliverMessage(addr, recipients, msg.Text)
			if err != nil {
				d.Reject(true)
				log.Printf("SMTP error: %v", err)
			} else {
				log.Printf("Sent mlf2_%d status to %q", int(floatid), recipients)
				d.Ack(true)
			}
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("parse config file: %v", err)
	}

	db := NewDb()
	dbfile, err := os.Open(args[1])
	if err != nil {
		log.Fatalf("open db file: %v", err)
	}

	if err = db.Load(dbfile); err != nil {
		log.Fatalf("read db file: %v", err)
	}
	dbfile.Close()

	if cfg.Mq.Vhost == "" {
		cfg.Mq.Vhost = "mlf2"
	}

	if cfg.Mq.Exchange == "" {
		cfg.Mq.Exchange = "data"
	}

	if cfg.Smtp.Mailhost == "" {
		cfg.Smtp.Mailhost = os.Getenv("MAILHOST")
		if cfg.Smtp.Mailhost == "" {
			log.Fatal("No mailhost specified")
		}
	}

	consumer, err := mq.NewConsumer(cfg.Mq.Uri(), cfg.Mq.Exchange, "topic")
	if err != nil {
		log.Fatalf("AMQP connection: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	dh := func(ch <-chan amqp.Delivery) {
		handleMessage(cfg.Smtp.Mailhost, db, ch)
	}

	err = consumer.Start("email", "", *bindkey, dh)
	if err != nil {
		log.Fatalf("Message consumer: %v", err)
	}

	// Shutdown the AMQP consumer on a signal interupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			consumer.Shutdown()
		}
	}()

	// Wait for the consumer to exit
	consumer.Wait()
}
