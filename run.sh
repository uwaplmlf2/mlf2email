#!/usr/bin/env bash

: ${IMAGE=mlf2/mlf2email:latest}

# Get IP address of Docker bridge
addr=$(ip addr show docker0 | awk '/inet / {split($2,s,"/");print s[1]}')

docker stop mlf2-email
docker rm mlf2-email

docker run --restart=unless-stopped --name=mlf2-email \
       --log-driver=journald \
       -v "$PWD:/config" \
       -e MAILHOST=${addr}:25 \
       -d $IMAGE /config/mlf2email.toml /config/emaildb.txt
