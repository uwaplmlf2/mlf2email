package main

import (
	"bytes"
	"testing"
)

var CONTENTS = `# MLF2 email list
42: mikek@apl.uw.edu   foo@bar.com
31:   joeuser@example.com
# The next entry is empty
0:
`

func TestLoad(t *testing.T) {
	buf := bytes.NewBuffer([]byte(CONTENTS))
	db := NewDb()
	err := db.Load(buf)
	if err != nil {
		t.Fatal(err)
	}

	entry := db.Lookup(42)
	if len(entry) != 2 {
		t.Fatalf("Wrong entry size; expected 2, got %d", len(entry))
	}

	if entry[1] != "foo@bar.com" {
		t.Errorf("Bad entry: %q", entry[1])
	}
}

func TestSave(t *testing.T) {
	var obuf bytes.Buffer
	ibuf := bytes.NewBuffer([]byte(CONTENTS))
	db := NewDb()
	err := db.Load(ibuf)
	if err != nil {
		t.Fatal(err)
	}

	db.Save(&obuf)

	// The entries are sorted on Save so 0 should be
	// the first entry.
	val := string(obuf.Bytes()[0:2])
	if val != "0:" {
		t.Errorf("Bad first entry: %q", val)
	}
}
