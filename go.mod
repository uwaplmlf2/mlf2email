module bitbucket.org/uwaplmlf2/mlf2email

go 1.13

require (
	bitbucket.org/uwaplmlf2/go-mlf2 v1.5.2
	github.com/BurntSushi/toml v0.3.1
	github.com/pkg/errors v0.8.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
)
